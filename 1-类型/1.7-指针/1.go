package main

import "fmt"

//支持指针类型*T,指针的指针**T,以及包含包名前缀*<package>.T
/**
1.默认nil,没有NULL常量
2.操作符"&"取变量地址,"*"透过指针访问目标对象
3.不支持指针运算,不支持"->"运算符,直接用"."访问目标成员
 */
func main()  {
	type data struct {
		a int
	}
	var d=data{1234}
	var p *data
	p=&d
	fmt.Printf("%p,%v\n",p,p.a)//直接使用指针访问目标对象成员,无须转换
}
